# [staticsitegenerators](https://www.npmjs.com/package/staticsitegenerators)

A comprehensive, partially automatically generated comparison of static site generators https://staticsitegenerators.net

* [Comparison of documentation generators](https://en.wikipedia.org/wiki/Comparison_of_documentation_generators) (WikipediA)
* https://staticgen.com
* https://google.com/search?q=awsome+static+site+generator
* https://github.com/myles/awesome-static-generators
* https://github.com/rogeriopradoj/awesome-static-site-generator

---
![Debian popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=mkdocs+python3-sphinx+hugo+jekyll+ikiwiki+pelican+nanoc+chronicle+libghc-hakyll-dev+blag&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Debian popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=nanoc+chronicle+libghc-hakyll-dev+blag&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* Hugo
* Mkdocs
* Jekyll
* Pelican
* Nanoc
* Ikiwiki
* Chronicle
* Hakyll
* Blag

# [*GitLab Pages examples*](https://gitlab.com/pages?sort=stars_desc) (by stars)
* https://tracker.debian.org/pkg/hugo
* https://tracker.debian.org/pkg/jekyll
* https://gitlab.com/javascript-packages-demo/hexo
* https://tracker.debian.org/pkg/pelican
* https://tracker.debian.org/pkg/sphinx
* https://gitlab.com/javascript-packages-demo/gatsby
* https://gitlab.com/javascript-packages-demo/nuxt.js
* https://tracker.debian.org/pkg/python-mkdocs
* https://www.staticgen.com/vuepress
* https://gitlab.com/pages/emacs-reveal
* https://tracker.debian.org/pkg/doxygen
* https://tracker.debian.org/pkg/nikola
* https://gitlab.com/javascript-packages-demo/metalsmith
---
By GitLab Pages stars

# Elixir
* https://gitlab.com/elixir-packages-demo/obelisk
* https://gitlab.com/elixir-packages-demo/serum
* https://gitlab.com/elixir-packages-demo/still

# Elm
* https://github.com/alexkorban/elmstatic

# Gleam
* [lustre_ssg](https://hex.pm/packages/lustre_ssg) https://github.com/lustre-labs/ssg

# Golang
* https://gitlab.com/hub-docker-com-demo/hugo
  * Books
    *  Hugo in Action: Static sites and dynamic Jamstack apps 2022 Atishay Jain

# Haskell
* https://gitlab.com/haskell-packages-demo/hakyll

# Javascript
* https://gitlab.com/javascript-packages-demo/hexo
* https://gitlab.com/javascript-packages-demo/gatsby
* https://gitlab.com/javascript-packages-demo/nuxt.js
* https://www.staticgen.com/vuepress
* https://gitlab.com/javascript-packages-demo/metalsmith
---
* https://gitlab.com/javascript-packages-demo/assemble
* https://gitlab.com/javascript-packages-demo/static-site-generator-webpack-plugin
* https://gitlab.com/javascript-packages-demo/wintersmith

# LISP
## Clojure
* Cryogen
  * https://www.staticgen.com/cryogen
* Perun
  * https://www.staticgen.com/perun
  * https://github.com/hashobject/perun
  * Inspired by Boot task model and Metalsmith.
* Stasis
  * https://github.com/magnars/stasis
  * Collection of functions that are useful when creating static web sites
  * Constains a list of static web sites "frameworks"
* https://google.com/search?q=clojure+static+site+generato

## Common Lisp
* https://www.staticgen.com/coleslaw
  * https://github.com/kingcons/coleslaw
* https://awesome-cl.com/#static-site-generators

## [Emacs Lisp](https://en.wikipedia.org/wiki/Emacs_Lisp)
* https://gitlab.com/pages/emacs-reveal

# Perl
* [perl static site generator](https://google.com/search?q=perl+static+site+generator)
* https://tracker.debian.org/pkg/ikiwiki
* https://tracker.debian.org/pkg/chronicle

# PHP
* https://gitlab.com/php-packages-demo/sculpin
* https://gitlab.com/php-packages-demo/spress
* https://gitlab.com/php-packages-demo/jigsaw

# Python
* https://gitlab.com/python-packages-demo/pelican
* https://gitlab.com/python-packages-demo/sphinx
  * [python:sphinx](https://repology.org/project/python:sphinx/versions)
    (Repology)
* https://tracker.debian.org/pkg/python-mkdocs
* https://tracker.debian.org/pkg/blag
---
* https://tracker.debian.org/pkg/nikola

# Ruby
* https://tracker.debian.org/pkg/jekyll
* https://tracker.debian.org/pkg/nanoc
* https://gitlab.com/ruby-packages-demo/middleman

# Rust
* https://gitlab.com/rust-packages-demo/zola
* https://gitlab.com/rust-packages-demo/mdblog
* https://gitlab.com/rust-packages-demo/cobalt-bin

# Docker
* https://gitlab.com/hub-docker-com-demo/hugo

# Utilities
* https://gitlab.com/javascript-packages-demo/katex
* https://gitlab.com/javascript-packages-demo/mermaid
* https://gitlab.com/javascript-packages-demo/lunr

# Indexes
* https://staticgen.com

# About
* [*7 Reasons NOT to Use a Static Site Generator — SitePoint*](https://www.sitepoint.com/7-reasons-not-use-static-site-generator/)
* https://google.com/search?q=Static+Site+Generator

---
![Static site generators popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-sphinxdoc%20doxygen%20python3-sphinx&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

![Static site generators popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=hugo%20jekyll%20pelican%20mkdocs&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

![Static site generators popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=nanoc%20staticsite%20nikola&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)
